## Теоретичні питання

1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування.

\ (зворотній слеш) служить для коректного відображення рядка з спецсимволами. Наприклад, щоб символ ' відображався як звичайний текст, а не означав закінчення рядка.

2. Які засоби оголошення функцій ви знаєте?

**Function Declaration** - пишемо слово function і назву функції, далі параметри і тіло функції.

function nameOfFunction() {
};

**Function Expression** - створює функцію і записує її в змінну. Пишемо назву змінної = function, параметри і тіло функції. Не можна використовувати до оголошення.

const variable = function() {
};

3. Що таке hoisting, як він працює для змінних та функцій?

**Hoisting (підйом)** — це механізм JavaScript, в якому змінні та функції, можуть використовуватися раніше, ніж були оголошені в коді.
Завдяки цьому ми можемо використовувати змінні до їх оголошення, але вони будуть мати значення undefine, тому варто оголошувати та надавати значенні змінним до їх використання.
У випадку з функціями, оголошення через Function Declaration дає нам можливість без проблем використовувати їх до фактично опису, але у випадку Function Expression, ми можемо їх використовувати тількі після ініціалізації.

