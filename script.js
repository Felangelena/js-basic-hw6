"use strict";

function createNewUser() {
    return {
        firstName: prompt("Please enter your name", ''),
        lastName: prompt("Please enter your last name", ''),
        birthday: prompt("Please enter your date of birth in format dd.mm.yyyy", 'dd.mm.yyyy'),
        getLogin() {
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        },
        getBirthdayDate() {
            //const d = new Date("2022-03-25");
            return this.birthday.split(".").reverse().join("-");
        },
        getAge() {
            const now = new Date ();
            const birthday = new Date(this.getBirthdayDate());
            const diff = new Date(now - birthday);
            return Math.abs(diff.getFullYear() - 1970);
        },
        getPassword(){
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(6);
        }
    };
}

const newUser = createNewUser();
console.log(newUser.getLogin());
console.log(newUser.getAge());
console.log(newUser.getPassword());